import logo from './logo.svg';
import './App.css';
import Header from './BaiTapLayoutComponent/Header';
import Body from './BaiTapLayoutComponent/Body';
import Footer from './BaiTapLayoutComponent/Footer';


function App() {
  return (
    <div className="App">
      <Header></Header>
      <div className='mt-5'>
        <Body></Body>
      </div>
      <Footer></Footer>
    </div>
  );
}

export default App;
