import React, { Component } from 'react'

export default class Item extends Component {
  render() {
    return (
        <div>
        <div
          style={{ position: "relative" }}
          className="card bg-light border-0 h-100 mt-5"
        >
          <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
            <div
              style={{
                lineHeight: "4rem",
                height: "4rem",
                width: "4rem",
                borderRadius: "5px",
                position: "absolute",
                bottom: "8rem",
                right: "50%",
                transform: "translateX(50%)",
              }}
              className="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4"
            >
              <i
                className="bi bi-bootstrap"
                style={{
                  fontSize: "2rem",
                }}
              />
            </div>
            <h2 className="" style={{ fontSize: "1.5rem", fontWeight: "700" }}>
              Feature boxes
            </h2>
            <p className="mb-0">
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>
      </div>
    )
  }
}
