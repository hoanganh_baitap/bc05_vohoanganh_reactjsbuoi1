import React, { Component } from 'react'
import Banner from './Banner'
import Item from './Item'

export default class Body extends Component {
  render() {
    return (
        <div>
        <Banner></Banner>
        <div className="container">
          <div className="row my-5">
            <div className="col-4">
              <Item></Item>
            </div>
            <div className="col-4">
              <Item></Item>
            </div>
            <div className="col-4">
              <Item></Item>
            </div>
            <div className="col-4">
              <Item></Item>
            </div>
            <div className="col-4">
              <Item></Item>
            </div>
            <div className="col-4">
              <Item></Item>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
