import React, { Component } from 'react'

export default class Banner extends Component {
  render() {
    return (
      <>
        <div className="container">
        <div className="card bg-light" style={{ border: "none" }}>
          <div className="card-body">
            <h1
              className="card-title mt-5 font-weight-bolder"
              style={{ fontSize: "3rem" }}
            >
              A warm welcome!
            </h1>
            <h4 className="card-text font-weight-normal">
              Bootstrap utility classes are used to create this jumbotron since
              the old component has been removed from the framework. Why create
              custom CSS when you can use utilities?
            </h4>
            <button href="#" className="btn btn-primary mt-3 mb-5 rounded">
              <span className="" style={{ fontSize: "1.3rem" }}>
                Call to action
              </span>
            </button>
          </div>
        </div>
      </div>
      </>
    )
  }
}
